"use strict"

const MINNUMBER = 1;
const MAXNUMBER = 100;

var answer = Math.floor(Math.random() * 100) + 1;
var guessNumber = 0;


var $ = function(id){
  return document.getElementById(id);
};

var processEntries = function() {
  var guess = parseFloat($("guess").value);
  var validInput = true;
  var guessStatus = "";


  if (isNaN(guess)){
    $("guess").nextElementSibling.firstChild.nodeValue = "Numeric input required";
    validInput = false;
  }
  else if ((guess < MINNUMBER) || (guess > MAXNUMBER)){
    $("guess").nextElementSibling.firstChild.nodeValue = "Guess input out of range. Must be between 1 and 100";
    validInput = false;
  }

  if (validInput){
    $("guess").nextElementSibling.firstChild.nodeValue = "";
    if (guess < answer){
      var guessStatus = ("Wrong! " + guess + " is too low");
      $("guessStatus").value = guessStatus;
      guessNumber++;
    }
    else if (guess > answer){
      var guessStatus = ("Wrong! " + guess + " is too high");
      $("guessStatus").value = guessStatus;
      guessNumber++;
    }
    else{
      $("guess").disabled = true;
      guessNumber++;
      var guessStatus = ("Guess " + answer + " correct in " + guessNumber + " guess(es)!");
      $("guessStatus").value = guessStatus;
    }
  }

};

var resetTheForm = function() {
  $("guess").value = "";
  $("guess").disabled = false;
  $("guessStatus").value = "";
  $("guess").nextElementSibling.firstChild.nodeValue = "*";
  $("guess").focus();

}

window.onload=function(){
  $("check").onclick = processEntries;
  $("reset").onclick = resetTheForm;
  $("guess").focus();
};
