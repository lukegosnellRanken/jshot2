"use strict"

const MINGALLONS = 1;
const MAXGALLONS = 100;
const MINMILES = 1;
const MAXMILES = 1000;
const MINPPG = 1.00;
const MAXPPG = 5.00;

var $ = function(id){
  return document.getElementById(id);
};

var processEntries = function() {
  var miles = parseFloat($("miles").value);
  var gallons = parseFloat($("gallons").value);
  var pricePerGallon = parseFloat($("pricePerGallon").value);
  var validInput = true;

  if (isNaN(miles)){
    $("miles").nextElementSibling.firstChild.nodeValue = "Numeric input required";
    validInput = false;
  }
  else if ((miles < MINMILES) || (miles > MAXMILES)){
    $("miles").nextElementSibling.firstChild.nodeValue = "Mile input out of range";
    validInput = false;
  }
  if (isNaN(gallons)){
    $("gallons").nextElementSibling.firstChild.nodeValue = "Numeric input required";
    validInput = false;
  }
  else if ((gallons < MINGALLONS) || (gallons > MAXGALLONS)){
    $("gallons").nextElementSibling.firstChild.nodeValue = "Gallon input out of range";
    validInput = false;
  }
  if (isNaN(pricePerGallon)){
    $("pricePerGallon").nextElementSibling.firstChild.nodeValue = "Numeric input required";
    validInput = false;
  }
  else if ((pricePerGallon < MINPPG) || (pricePerGallon > MAXPPG)){
    $("pricePerGallon").nextElementSibling.firstChild.nodeValue = "PPG input out of range";
    validInput = false;
  }

  if (validInput){
    calculateMPG(miles, gallons);
    calculateTripCost(gallons, pricePerGallon);
    calculatePPM(gallons, pricePerGallon, miles);
  }

};

var calculateMPG = function(m, g){
  var mpg = (m /g).toFixed(1);
  $("mpg").value = mpg;
};

var calculateTripCost = function(g, p){
  var tripCost = (g * p).toFixed(2);
  $("tripCost").value = tripCost;
};

var calculatePPM = function(g, p, m){
  var pricePerMile = ((g * p) / m).toFixed(2);
  $("pricePerMile").value = pricePerMile;
};

var resetTheForm = function() {
  $("miles").value = "";
  $("gallons").value = "";
  $("pricePerGallon").value = "";
  $("mpg").value = "";
  $("tripCost").value = "";
  $("pricePerMile").value = "";
  $("miles").nextElementSibling.firstChild.nodeValue = "*";
  $("gallons").nextElementSibling.firstChild.nodeValue = "*";
  $("pricePerGallon").nextElementSibling.firstChild.nodeValue = "*";
  $("miles").focus();

}

window.onload=function(){
  $("calculate").onclick = processEntries;
  $("reset").onclick = resetTheForm;
  $("miles").focus();
};
